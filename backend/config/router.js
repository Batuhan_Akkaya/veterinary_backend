const ClinicController = require('../controllers/clinic'),
      AuthenticationController = require('../controllers/authentication'),
      PostController = require('../controllers/post'),
      express = require('express'),
      uploader = require('../config/uploader');

module.exports = function (app) {
    const apiRoutes = express.Router(),
          clinicRoutes = express.Router(),
          authRoutes = express.Router(),
          postRoutes = express.Router();

    //= ========================
    // Auth Routes
    //= ========================
    apiRoutes.use('/auth', authRoutes);

    authRoutes.post('/register', AuthenticationController.register);
    authRoutes.post('/login', AuthenticationController.login);

    //= ========================
    // Clinic Routes
    //= ========================
    apiRoutes.use('/clinic', clinicRoutes);

    clinicRoutes.get('/getAll', ClinicController.getAll);
    clinicRoutes.get('/getByCity/:city', ClinicController.getByCity);
    clinicRoutes.get('/getById/:id', ClinicController.getById);

    //= ========================
    // Post Routes
    //= ========================
    apiRoutes.use('/post', postRoutes);

    postRoutes.get('/getAll', PostController.getAll);
    postRoutes.get('/getById/:id', PostController.getById);
    postRoutes.post('/create/:userId', uploader.upload.single('image'), PostController.create);
	postRoutes.get('/getOfUser/:userId', PostController.getOfUser);
	postRoutes.get('/getOfNest', PostController.getPostOfNest);
	postRoutes.get('/getOfLost', PostController.getPostOfLost);
	postRoutes.get('/getForList', PostController.getForList);
	postRoutes.post('/setActivation', PostController.setActivation);
	postRoutes.get('/getInactives', PostController.getInactives);

    app.use('/api', apiRoutes);
};