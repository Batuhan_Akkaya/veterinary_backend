const Clinic = require('../models/clinic');

exports.getAll = (req, res) => {
    Clinic.find().exec((err, clinics) => {
        if (!err) res.send(clinics);
    });
};

exports.getByCity = (req, res) => {
    Clinic.find({city: req.params.city}).exec((err, clinics) => {
        if (!err) res.send(clinics);
    });
};

exports.getById = (req, res) => {
    Clinic.findById(req.params.id).exec((err, clinic) => {
        if (!err) res.send(clinic);
    });
};