const User = require('../models/user'),
    setUserInfo = require('../config/helpers').setUserInfo;

//= =======================================
// Login Route
//= =======================================
exports.login = (req, res) => {
    User.findOne({ email: req.body.email, password: req.body.password }, (err, user) => {
        if (err) { return console.log(err); }
        if (!user)  res.status(422).send({login: 0, error: 'Please try again.' });
        else res.status(200).json({login: 1, id: user._id});
    });
};

//= =======================================
// Registration Route
//= =======================================
exports.register = function (req, res, next) {
    const {name, password, city} = req.body;

    const user = new User({
        name, password, city
    });

    user.save((err, user) => {
        if (err) return next(err);

        const userInfo = setUserInfo(user);
        res.status(201).json({user: userInfo});
    });
};