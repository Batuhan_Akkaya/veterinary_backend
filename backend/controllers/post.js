const Post = require('../models/post');
const sendActivationEmail = require('../config/helpers').sendActivationEmail;

exports.create = (req, res) => {
    const {title, description, type, city, contact} = req.query;
    const post = new Post({
        title, description, type, city, contact,
        user: req.params.userId,
        image: req.file.path
    });
    post.save().then(result => {
        res.send(result);
        sendActivationEmail();
    }).catch(err => {
        res.status(500).json({error: err});
    });
};

exports.getAll = (req, res) => {
    Post.find().exec((err, data) => {
        if (!err) res.send(data);
    })
};

exports.getById = (req, res) => {
    Post.findById(req.params.id).exec((err, data) => {
        if (!err) res.send(data);
    })
};

exports.getOfUser = (req, res) => {
    Post.find({user: req.params.userId}).exec((err, data) => {
        if (!err) res.send(data);
    })
};

exports.getPostOfNest = (req, res) => {
    Post.find({type: 'nest', active: true}).exec((err, data) => {
        if (!err) res.send(data);
    });
};

exports.getPostOfLost = (req, res) => {
    Post.find({type: 'lost', active: true}).exec((err, data) => {
        if (!err) res.send(data);
    });
}

exports.getForList = (req, res) => {
    Post.find({active: true}).exec((err, data) => {
        if (!err) res.send(data);
    });
}

exports.setActivation = (res, req) => {
    Post.findByIdAndUpdate(req.params.id, {$set: { active: req.body.value }}, { new: true }, (err, data) => {
        if (!err) res.send(data);
    });
}

exports.getInactives = (req, res) => {
    Post.find({active: false}).exec((err, data) => {
        if (!err) res.send(data);
    });
}