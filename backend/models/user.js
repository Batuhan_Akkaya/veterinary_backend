const mongoose = require('mongoose'),
    Schema   = mongoose.Schema;

const UserSchema = new Schema({
    name: {type: String, required: true},
    password: {type: String, required: true},
    city: {type: String, required: true}
}, {
    timestamps: true
});

module.exports = mongoose.model('User', UserSchema);