const mongoose = require('mongoose'),
      Schema = mongoose.Schema;

const PostSchema = new Schema({
    title: {type: String},
    description: {type: String},
    type: {type: String},
    city: {type: String},
    active: {type: Boolean, default: false},
    contact: {type: String},
    image: {type: String},
    user: {type: mongoose.Schema.Types.ObjectId, ref: 'User', required: true}
}, {
    timestamps: true
});

module.exports = mongoose.model('Post', PostSchema);