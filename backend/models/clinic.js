const mongoose = require('mongoose'),
      Schema   = mongoose.Schema;

const ClinicSchema = new Schema({
    title: {type: String},
    address: {type: String},
    country: {type: String},
    city: {type: String},
    tel: {type: String}
}, {
    timestamps: true
});

module.exports = mongoose.model('Clinic', ClinicSchema);