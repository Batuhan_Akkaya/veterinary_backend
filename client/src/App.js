import React, {Component} from 'react';
import logo from './logo.svg';
import './App.css';
import Ajax from 'axios';

class App extends Component {
	constructor(props) {
		super(props);
		this.state = {
			selectedFile: null,
			title: 'Başlık denemek',
			description: 'Lorem ipsum dolar sit ameter, Lorem ipsum dolar sit ameter, Lorem ipsum dolar sit ameter.',
			city: 'Konya',
			contact: 'Numaram: 05395748294',
			type: 'nest'
		}
	}

	doPost() {
		const {title, description, city, contact, type} = this.state;
		const formData = new FormData();
		formData.append('image', this.state.selectedFile, this.state.selectedFile.name);
		let url = `http://localhost:3100/api/post/create/5ac61bb101692c2b20f7b849?title=${title}&description=${description}&city=${city}&contact=${contact}&type=${type}`;
		Ajax.post(url, formData).then(res => {
			console.log(res);
		}).catch(err => console.log(err));
	}

	handler(e) {
		this.setState({selectedFile: e.target.files[0]});
	}

	render() {
		const {type, title, description, city, contact} = this.state;
		return (
			<div className="App">
				<h2>New Post</h2>
				<input type="file" onChange={e => this.handler(e)}/>
				<input type="text" value={title} onChange={e => this.setState({title: e.target.value})} />
				<input type="text" value={description} onChange={e => this.setState({description: e.target.value})} />
				<input type="text" value={city} onChange={e => this.setState({city: e.target.value})} />
				<input type="text" value={contact} onChange={e => this.setState({contact: e.target.value})} />
				<input type="text" value={type} onChange={e => this.setState({type: e.target.value})} />
				<button onClick={() => this.doPost()}>Gönder</button>
			</div>
		);
	}
}

export default App;
